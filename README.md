# m6-ijf

### Introduction

This repository contains the scripts to reproduce the results obtained by the team AdaGaussMC in the [M6 Financial Forecasting Competition](https://www.unic.ac.cy/iff/research/forecasting/m-competitions/m6/). The results are presented in the article [An adaptive volatility method for probabilistic forecasting and its application to the M6 financial forecasting competition](https://arxiv.org/abs/2303.01855).

### Structure

- Our original submissions in the M6 competition are in the submissions folder (csv files).
- M6 universe and financial data are in the data folder.
- The code is in the Scripts folder (see main.R and adavol.ipynb).